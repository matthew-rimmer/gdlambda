extends Object
class_name LambdaParser

# String vars
var lambdaString
var splitLambdaString

# Context stack
var contexts = []

var variables = []


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _init(var stringIn):
	lambdaString = stringIn
	getSplitLambdaString()
	
func getSplitLambdaString():
	var regex = RegEx.new()
	regex.compile("[λa-z.()]")
	splitLambdaString = regex.search_all(lambdaString)
	
	for i in splitLambdaString:
		print(i.get_string())
		
	lexAnalyse()


func lexAnalyse():
	
	var workingList = []
	
	for x in range(len(splitLambdaString)):
		workingList.append(splitLambdaString[x].get_string())
		
	
	
	var workingString =  PoolStringArray(workingList).join("")
	
	var testArray = []
	
	var placeInString = 0
	
	var xString = workingString[placeInString]
	
	testArray = parse(workingString)
	
	#print(testArray[0][0])
	#print(testArray[0][1])
	print("Parsed value: ", testArray)
	
	print(testArray[0][1])

func findScope(var stringIn):
	var stringArray = []
	for c in stringIn:
		stringArray.append(c)
	
	var lambdaStatement = []
	var atEnd = false
	var currentIndex = 0
	var result = ""
	while !atEnd:
		if stringArray[currentIndex] == "(":
			var contextNumber = 0
			for y in range(currentIndex+1, stringArray.size()):
				print(stringArray[y])
				if stringArray[y] == '(':
					contextNumber+=1
				elif stringArray[y]==')':
					if contextNumber == 0:
						print("FoundEnd")
						result = arrayToStr(stringArray.slice(currentIndex+1,y-1,true))
						if y == stringIn.length()-1:
							pass
						else:
							var stringEnd = stringArray.slice(y+1,stringIn.length(),true)
							var resultAarry = []
							resultAarry += [result]
							var endResult = findScope(arrayToStr(stringEnd))
							if typeof(endResult) == TYPE_ARRAY:
								resultAarry += endResult
							else:
								resultAarry.append(endResult)
							return resultAarry
							
						atEnd = true
					else:
						contextNumber -=1
	print(result)
	return result





func arrayToStr(var arrayIn):
	var string = ""
	for i in range(0,arrayIn.size()):
		string += arrayIn[i]
	return string

func parse(var stringIn):
	
	#var xString = stringIn[0]
	
	var stringArray = []
	for c in stringIn:
		stringArray.append(c)
	print(stringArray)
	
	# If we find a bracket
	if stringIn.begins_with('('):
		var scope = findScope(stringIn)
		
		if typeof(scope) == TYPE_ARRAY:
			var scopeList = []
			for x in scope:
				scopeList.append(parse(x))
			return scopeList
		else:
			return [parse(scope)]
	elif stringIn.begins_with('λ'):
			print('Func Found')
			var stringEndDef = stringIn.find(".",0)
			var subStringDef = stringIn.substr(0, stringEndDef-0)
			var place = stringEndDef
			print("Function definition: ",subStringDef)
			
			var stringStartBod = stringIn.find("(",stringEndDef)
			var stringEndBod = stringIn.find(")",stringEndDef)
			var subStringBod = stringIn.substr(stringStartBod+1, stringEndBod-place-2)
			print("Function body: ",subStringBod)
			
			var functionArray = [subStringDef,[subStringBod]]
			if subStringBod.length() == 1:
				return functionArray
			else:
				return [subStringDef, parse(subStringBod)]
	else:
		return [stringIn]

func reduce():
	return lambdaString

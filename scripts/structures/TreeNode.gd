extends Object
class_name TreeNode

var _parent : TreeNode = null
var _children : = [] 
var _string


func _init(var stringIn):
	_string = stringIn

func AddChild(var stringIn):
	var leaf = get_script().new(stringIn)
	_children.append(leaf)

func PrintAll():
	print(_string)
	for child in _children:
		child.PrintAll()

func ReturnAll():
	var outList = []
	print(_string)
	outList.append(_string)
	for child in _children:
		outList.append(child.ReturnAll())
		
	return outList

func _notification(p_what):
	match p_what:
		NOTIFICATION_PREDELETE:
			# Destructor.
			for a_child in _children:
				a_child.free()

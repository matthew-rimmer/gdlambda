extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var Leaf = load("res://scenes/TreeGraph/Root.tscn")

var tree

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func _init(var treeIn):
	var tree = treeIn
	
func gen(var num):
	reset()
	add_leaves(tree)
	var time_in_seconds = 1
	yield(get_tree().create_timer(0.01), "timeout")
	check_lines()

func clear():
	for child in $CenterContainer.get_children():
		$CenterContainer.remove_child(child)
		child.queue_free()
	current_root = $CenterContainer
	clear_lines()
		
func clear_lines():
	var nodes = get_tree().get_nodes_in_group("lines")
	if len(nodes) != 0:
		for node in nodes:
			remove_child(node)
			node.queue_free()

onready var current_root = $CenterContainer

func add_leaves(root):
	var rootScene = Leaf.instance()
	rootScene.get_node("RootLabel").text = root._string
	#rootScene.get_node("RootLabel").z_index = 1
	if current_root == $CenterContainer:
		current_root.add_child(rootScene)
	else:
		current_root.get_node("LeavesContainer").add_child(rootScene)
	#print(rootScene.get_parent().name)
	
	
	for child in root._children:
		print(root._children)
		current_root = rootScene
		add_leaves(child)


func check_lines():
	var nodes = get_tree().get_nodes_in_group("RootNode")
	if len(nodes) != 0:
		for node in nodes:
			add_lines(node)
		
func add_lines(root):
	var rootLabel = root.get_node("RootLabel")
	var leaves = root.get_node("LeavesContainer").get_children()
	for leaf in leaves:
		draw_line2D(rootLabel, leaf.get_node("RootLabel"))
		
func get_center(var node):
	var center = Vector2((node.rect_global_position.x + node.rect_size.x/2 - rect_global_position.x)/rect_scale.x, 
						(node.rect_global_position.y + node.rect_size.y/2 - rect_global_position.y)/rect_scale.y)
	return center
	
	
func draw_line2D(var node1, var node2):
	var line = Line2D.new()
	line.width=3
	line.add_to_group("lines")
	line.default_color = Color(0,0,0)
	line.show_behind_parent = true
	line.antialiased = true
	line.add_point(get_center(node1))
	line.add_point(get_center(node2))
	add_child(line)



	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_TreeGraph_resized():
	clear_lines()
	yield(get_tree().create_timer(0.01), "timeout")
	check_lines()


func reset():
	clear()
	clear_lines()
	rect_position = Vector2(0, 0)
	rect_scale = Vector2(1,1)

var mouseDown = false
var mouseOrigin = Vector2(0,0)

var lastX = 0
var lastY = 0

var newX = 0
var newY = 0


func _input(event):
	if event is InputEventMouseButton:
		# zoom in
		if event.button_index == BUTTON_WHEEL_UP:
			rect_scale =  rect_scale + Vector2(0.1, 0.1)
			# call the zoom function
		# zoom out
		if event.button_index == BUTTON_WHEEL_DOWN:
			# call the zoom function
			rect_scale =  rect_scale + Vector2(-0.1, -0.1)
	
	if (event is InputEventMouseButton and event.button_index == BUTTON_LEFT):
		if mouseDown:
			mouseDown = false
			#print("mouseUp")
			var mousepos = get_viewport().get_mouse_position()
			lastX = (lastX + mousepos.x - mouseOrigin.x)
			lastY = (lastY + mousepos.y - mouseOrigin.y)
			newX = 0
			newY = 0
		else:
			mouseDown = true
			#print("mouseDown")
			#print("Mouse origin ", mouseOrigin.x, " ", mouseOrigin.y)
			#print($Space.rect_position.x, " ", $Space.rect_position.y)
			mouseOrigin = get_viewport().get_mouse_position()	
			
	if event is InputEventMouseMotion and mouseDown:
		var mousepos = get_viewport().get_mouse_position()
		newX = (rect_position.x + lastX +(mousepos.x - mouseOrigin.x))/2
		newY = (rect_position.y + lastY + (mousepos.y - mouseOrigin.y))/2
		#print("Change in pos ", (mousepos.x - mouseOrigin.x)," ", (mousepos.y - mouseOrigin.y))
		#print("Rect ", $Space.rect_position.x, " ", $Space.rect_position.y)
		#if $Space.rect_position.x == 0 or $Space.rect_position.y == 0 or mousepos.x == 0 or mousepos.y == 0:
		#	print("ZERO")
		rect_position = Vector2(newX, newY)
	

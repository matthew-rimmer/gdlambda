extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


onready var LambdaLineEdit = $Panel/MarginContainer/VBoxContainer/HBoxContainer/LineEdit
onready var LambdaRichText = $Panel/MarginContainer/VBoxContainer/MarginContainer/RichTextLabel

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func _process(delta):
	pass


func _input(event):
	if event.is_action_pressed("ui_accept"):
		var calc = LambdaParser.new(LambdaLineEdit.text)
		LambdaRichText.text = calc.reduce()
		print(calc.reduce())
	elif event.is_action("#"):
		var temp_position = LambdaLineEdit.caret_position
		LambdaLineEdit.text = LambdaLineEdit.text.replace('#','\u03BB')
		LambdaLineEdit.caret_position = temp_position

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
